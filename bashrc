##############################################################################
# Shell prompt
#export PS1="\n\[\e[0;36m\]┌─[\[\e[0m\]\[\e[1;33m\]\u\[\e[0m\]\[\e[1;36m\] @ \[\e[0m\]\[\e[1;33m\]\h\[\e[0m\]\[\e[0;36m\]]─[\[\e[0m\]\[\e[1;34m\]\w\[\e[0m\]\[\e[0;36m\]]\[\e[0;36m\]─[\[\e[0m\]\[\e[0;31m\]\t\[\e[0m\]\[\e[0;36m\]]\[\e[0m\]\n\[\e[0;36m\]└─[\[\e[0m\]\[\e[1;37m\]\$\[\e[0m\]\[\e[0;36m\]]› \[\e[0m\]"

# Some env specifics
source ~/dotfiles/bash_prompt
source ~/dotfiles/homebrew
source ~/dotfiles/pyenv
source ~/dotfiles/pipx
source ~/dotfiles/awsenv
source ~/dotfiles/pixenvrc

# If fortune is installed, run a fortune
if [ -e /opt/local/bin/fortune ]; then
    fortune -so
    echo " "
fi

export EDITOR='/usr/bin/vim'
export GROOVY_HOME=/usr/local/opt/groovy/libexec

##############################################################################
# 02. Aliases                                                                #
alias fuck='sudo $(history -p \!\!)'

# Chef
alias chefdk='eval "$(chef shell-init bash)"'

# Personal
alias gotun='ssh -D localhost:4242 orie@trebuchet.blacklight.net'
alias bltun='ssh -D localhost:4242 orie@blacklight.net'
alias etun='ssh -D localhost:4242 root@earth.dynamic.blacklight.net'
alias sshbl='ssh orie@blacklight.net'
alias dtun='ssh -p 60022 -D localhost:4242 -L 9091:10.128.0.2:9091 -L 8096:10.128.0.2:8096 -L 1234:10.128.0.4:80 -L 1235:10.128.0.6:80 orie@earth.dynamic.blacklight.net'
alias dltun='ssh -D 4242 orie@10.128.0.2'
alias ctun='ssh -D localhost:4242 -L 9091:10.128.0.2:9091 -L 8096:10.128.0.2:8096 -L 1234:10.128.0.4:80 -L 1235:10.128.0.6:80 orie@earth.dynamic.blacklight.net'

alias db1='ssh efreis-ops@drawbridge-internal.pixsystem.io'
alias cb1='sudo ssh -i ~efreis/.ssh/id_rsa -L 443:10.128.250.37:443 -L 1443:10.128.247.17:443 -L 2443:10.128.249.5:443 -L 15671:10.128.32.22:15671 efreis-ops@drawbridge-internal.pixsystem.io'
alias sshcorp='ssh -i ~/.ssh/CorpVPCdefaultKey.pem'
alias dt="sudo pkill ScreenSaverEngine;/System/Library/Frameworks/ScreenSaver.framework/Resources/ScreenSaverEngine.app/Contents/MacOS/ScreenSaverEngine -background &"

##############################################################################
# Enable colors in "ls" command output
alias ls="ls -Glah"

##############################################################################
# 03. Theme/Colors                                                           #
##############################################################################
# CLI Colors
export CLICOLOR=1
# Set "ls" colors
#export LSCOLORS=Gxfxcxdxbxegedabagacad
export LSCOLORS=dxfxcxdxbxegedabagacad

##############################################################################
# 04. PATH
##############################################################################
PATH=:/usr/local/bin:/usr/local/sbin:~/bin:$PATH

##############################################################################
# 05. HISTORY
##############################################################################

# Eternal bash history.
# ---------------------
# Undocumented feature which sets the size to "unlimited".
# http://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"

eval "$(direnv hook bash)"

# Terraform
complete -C /opt/homebrew/bin/terraform terraform
