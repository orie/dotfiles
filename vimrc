" Ok, let's load some shit up
execute pathogen#infect()

" And make it right
set nocompatible                  " VI ain't gunna cut it
colorscheme elflord               " Stupid name, love the theme

" Below here be non-tab stuff, arrr
set expandtab                     " Tabs are never used right, to heck with 'em
set softtabstop=2                 " 2, 4, whatever!
set shiftwidth=2                  " What ^ said

" And below here be tab stuff.  narrr
"set tabstop=4
"set softtabstop=4
"set shiftwidth=4

let g:airline_powerline_fonts = 1 " For the pretty (be sure you have patched fonts)

" These are handled by my forked sensible and no longer necessary
"runtime macros/matchit.vim
"syntax on                         " Enable syntax highlighting
"filetype plugin indent on         " Cuz proper indenting is good
"set laststatus=2                  " Status lines are useful

" yaml stuff
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
